﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AkronikASP.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NinjaNye.SearchExtensions;

namespace AkronikASP.Models
{
    [RoutePrefix("Texte")]
    public class TexteController : Controller
    {
        public TextesContext context = new TextesContext();
        int MaxParPage = 3;

        public ActionResult Redaction()
        {
            return View();
        }

        // GET: Texte
        [Route("Index/{page=1}")]
        public ActionResult Index(int page)
        {
            ListeTexteViewModels listeTextes = new ListeTexteViewModels();
            int textesCount = context.Textes.Count();
            var textes = context.Textes
                .OrderBy(t => t.ID)
                .Skip((page-1)*MaxParPage)
                .Take(MaxParPage)
                .Select(
                t => new TexteViewModels()
                {
                    NomAuteur = t.Auteur.Nom,
                    PrenomAuteur = t.Auteur.Prenom,
                    NomTag = t.Tag,
                    Note = t.Note,
                    Contenu = t.Contenu,
                    Titre = t.Titre
                });
            int nombrePage = (textesCount / MaxParPage);
            if (textesCount % MaxParPage != 0) nombrePage += 1;
            listeTextes.PageCourante = page;
            listeTextes.NombrePage = nombrePage;
            listeTextes.ListeTextes = textes.ToList();
            return View(listeTextes);
        }

        [ValidateInput(false)]
        public ActionResult AjoutTexte(TexteViewModels texte)
        {
            if (ModelState.IsValid)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                if (userManager == null) return RedirectToAction("Login", "Account");
                if (User == null) return RedirectToAction("Login", "Account");
                var user = userManager.FindById(User.Identity.GetUserId());
                if (user == null) return RedirectToAction("Login", "Account");
                var monTexte = new Texte()
                {
                    Titre = texte.Titre,
                    Contenu = texte.Contenu,
                    Auteur = new Auteur
                    {
                        Username = user.UserName,
                        Nom = user.LastName,
                        Prenom = user.FirstName,
                        Email = user.Email
                    },
                    Tag = texte.NomTag,
                    Note = texte.Note,
                    Date = new Date()
                    {
                        Annee = 2016,
                        Jour = 16,
                        Mois = 7
                    }
                };
                context.Textes.Add(monTexte);
                context.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("Redaction", texte);
            }
        }

        public ActionResult Rechercher (string Recherche)
        {
            ListeTexteViewModels listeTextes = new ListeTexteViewModels();
            int textesCount = context.Textes.Count();

            var textes = context.Textes
                .Select(
                t => new TexteViewModels()
                {
                    NomAuteur = t.Auteur.Nom,
                    PrenomAuteur = t.Auteur.Prenom,
                    NomTag = t.Tag,
                    Note = t.Note,
                    Contenu = t.Contenu,
                    Titre = t.Titre
                });
            var result = textes.Search(x => x.Contenu,
                x => x.NomAuteur,
                x => x.NomTag,
                x => x.PrenomAuteur,
                x => x.Titre
                ).Containing(Recherche);

            listeTextes.ListeTextes = result.ToList();
            return View("Index", listeTextes);
        }
    }
}