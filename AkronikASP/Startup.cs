﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AkronikASP.Startup))]
namespace AkronikASP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
