﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AkronikASP.Models
{
    public class Texte
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Titre { get; set; }

        [Required]
        public string Contenu { get; set; }

        [Required]
        public Auteur Auteur { get; set; }

        [Required]
        public double Note { get; set; }

        [Required]
        public Date Date { get; set; }

        [Required]
        public string Tag { get; set; }

        public Texte()
        {
        }
    }
}
