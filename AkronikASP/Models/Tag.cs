﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AkronikASP.Models
{
    public class Tag
    {
        [Key]
        public int ID { get; set; }
        public string Texte { get; set; }

    }
}