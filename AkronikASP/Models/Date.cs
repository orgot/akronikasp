﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace AkronikASP.Models
{
    public class Date
    {
        [Key]
        public int ID { get; set; }

        public int Jour { get; set; }
        public int Mois { get; set; }
        public int Annee { get; set; }
        public int Heure { get; set; }
        public int Minute { get; set; }
        public int Seconde { get; set; }
    }
}