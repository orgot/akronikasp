﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AkronikASP.Models;
using System.Web.Mvc;

namespace AkronikASP.ViewModels
{
    public class TexteViewModels
    {
        [Required]
        [Display(Name = "Texte")]
        [AllowHtml]
        public string Contenu { get; set; }

        [Display(Name = "Nom Auteur")]
        public string NomAuteur { get; set; }

        [Display(Name = "Prenom Auteur")]
        public string PrenomAuteur { get; set; }

        [Required]
        [Display(Name = "Tag")]
        public string NomTag { get; set; }

        [Required]
        [Display(Name = "Note")]
        public double Note { get; set; }

        [Required]
        [Display(Name = "Titre")]
        public string Titre { get; set; }

        public TexteViewModels()
        {
        }
    }
   
    public class ListeTexteViewModels
    {
        public List<TexteViewModels> ListeTextes { get; set; }
        public int NombrePage { get; set; }
        public int PageCourante { get; set; }
        public ListeTexteViewModels()
        {
            ListeTextes = new List<TexteViewModels>();
        }
    }
}